= Constitution pour une Dictature parfaite

[source,bash]
----
[ x = mensonge ] && x=vérité
----

Le *Dictateur* est désigné par les dirigeants :

* des fabriques de tisanes à la camomille;
* des imprimeries de papier toilette.

== Les re-pouvoirs


. Le *Dictateur* nomme et révoque les ministres, les députés et les magistrats.
. Les ministres nomment et révoquent leurs fonctionnaires.
. Les médias ont tout loisir de faire l'éloge du Gouvernement.

'''
niktoa niktoo
== Dispositions diverses

. La [.underline]#liberté d'opinion# est protégée par la `Police de la pensée`.
. Les concours sont interdits et les quotas sont obligatoires.
. L'_Éducation nationale_ organise des activités exclusivement ludiques. 
. La _Vérité historique_ est r�digée par les deputes.
. Tout _dissident_ se verra administrer une dose de chacun des vaccins existants.
. Tout nourrisson _sans dents_ aussi.
. En cas de grippe, tous les [.line-through]#citoyens# _sujets_ seront assignés à r�sidence.

[TIP]
====
<<<<<<< HEAD
En manque d'inspiration ? Trouvez des idees sur http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/la-constitution/la-constitution-du-4-octobre-1958/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur.5074.html[un modèle réel].
=======
En manque d'inspiration ? Trouvez des id�es sur http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/la-constitution/la-constitution-du-4-octobre-1958/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur.5074.html[un modèle réel].
>>>>>>> upstream/master
====
